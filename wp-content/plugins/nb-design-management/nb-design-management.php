<?php
/**
 * @package Nbdesigner
 */
/*
Plugin Name: NB Design Management 4/5/2022
Plugin URI: https://cmsmart.net/
Description: Design Management Nbdesigner's design and upload files
Version: 1.0.0
Author: NetbaseTeam
Author URI: https://cmsmart.net/
License: GPLv2 or later
Text Domain: nb-manage-design
Domain Path: /langs
*/

$upload_dir = wp_upload_dir();
$basedir    = $upload_dir['basedir'];
$baseurl    = $upload_dir['baseurl'];
define( 'NB_CUSTOM_DATA_DIR',               $basedir . '/nbdesigner' );
define( 'NB_CUSTOM_DATA_URL',               $baseurl . '/nbdesigner' );
define( 'NB_CUSTOM_PLUGIN_URL',             plugin_dir_url( __FILE__ ) );
define( 'NB_CUSTOM_PLUGIN_DIR',             plugin_dir_path( __FILE__ ) );
define( 'NB_CUSTOM_ASSETS_URL',             NB_CUSTOM_PLUGIN_URL . 'assets/' );
define( 'NB_CUSTOM_JS_URL',                 NB_CUSTOM_PLUGIN_URL . 'assets/js/' );
define( 'NB_CUSTOM_CSS_URL',                NB_CUSTOM_PLUGIN_URL . 'assets/css/' );
define( 'NB_CUSTOM_ASSETS_DIR',             NB_CUSTOM_PLUGIN_DIR . 'assets/' );
define( 'NB_CUSTOM_JS_DIR',                 NB_CUSTOM_PLUGIN_DIR . 'assets/js/' );
define( 'NB_CUSTOM_CSS_DIR',                NB_CUSTOM_PLUGIN_DIR . 'assets/css/' );
define(' NBDESIGNER_BACKGROUND_DIR', 		NB_CUSTOM_DATA_DIR.'/backgrounds');
define(' NBDESIGNER_BACKGROUND_URL', 		NB_CUSTOM_DATA_URL.'/backgrounds');
require_once( NB_CUSTOM_PLUGIN_DIR . 'includes/class-admin-options.php' );
require_once( NB_CUSTOM_PLUGIN_DIR . 'includes/class-background-management.php' );
require_once( NB_CUSTOM_PLUGIN_DIR . 'includes/manager-design-rest-api/class-wc-rest-manager-design-controller.php' );