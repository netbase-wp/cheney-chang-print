<?php

/**
 * REST API Manager Design Controller
 *
 * Handles requests to the /orders endpoint.
 *
 */
defined( 'ABSPATH' ) || exit;

class WC_REST_Manager_Design_Controller {
    /**
     * You can extend this class with
     * WP_REST_Controller / WC_REST_Controller / WC_REST_Products_V2_Controller / WC_REST_CRUD_Controller etc.
     * Found in packages/woocommerce-rest-api/src/Controllers/
     */
    protected $namespace = 'wc/v3';

    protected $rest_base = 'custom';

    // protected $post_type = 'shop_order';

    public function __construct() {
        add_action( 'rest_api_init' , array( $this , 'register_routes') );
    }

    public function register_routes() {
        register_rest_route( $this->namespace, '/' . $this->rest_base . '/search',
            array(
                'methods'  => WP_REST_Server::READABLE,
                'callback' => array( $this, 'getListUsers' ),
                'permission_callback' => '__return_true',
            )
        );

        register_rest_route( $this->namespace, '/' . $this->rest_base . '/orders',
            array(
                'methods'  => WP_REST_Server::READABLE,
                'callback' => array( $this, 'getOrderByUsersId' ),
                'permission_callback' => '__return_true',
            )
        );
        register_rest_route( $this->namespace, '/' . $this->rest_base . '/folder',
            array(
                'methods'  => WP_REST_Server::READABLE,
                'callback' => array( $this, 'getListFolders' ),
                'permission_callback' => '__return_true',
            )
        ); 
        register_rest_route( $this->namespace, '/' . $this->rest_base . '/remove',
            array(
                // 'methods'  => WP_REST_Server::EDITABLE,
                'methods'  => WP_REST_Server::READABLE,
                'callback' => array( $this, 'removeFolderDesigns' ),
                'permission_callback' => '__return_true',
            )
        ); 
        register_rest_route( $this->namespace, '/' . $this->rest_base . '/log/(?P<id>[\d]+)',
            array(
                'methods'  => WP_REST_Server::READABLE,
                'callback' => array( $this, 'getLogByUserId' ),
                'permission_callback' => '__return_true',
            )
        ); 
        register_rest_route( $this->namespace, '/' . $this->rest_base . '/update-database',
            array(
                'methods'  => WP_REST_Server::READABLE,
                'callback' => array( $this, 'updateDatabase' ),
                'permission_callback' => '__return_true',
            )
        );
    }

    // update database 
    public function updateDatabase() {
        global $wpdb;
        $query_first    = "SELECT ID FROM wp_users AS u INNER JOIN wp_usermeta AS mt1 ON ( u.ID = mt1.user_id ) ";
        //custom update have_design update database
        $_query = $query_first." GROUP BY u.ID ORDER BY u.ID ASC";
        $_users          = $wpdb->get_results($_query);
        foreach( $_users AS $user ){
            $user_id    = $user->ID;
            $item       = $this->getDataUser($user_id , $index);
            $save_4_lates = $this->get_my_designs($user_id)['total'];
            if( $item['nbu_total'] > 0 || $item['nbd_total'] > 0 || $save_4_lates > 0) {
                update_user_meta( $user_id , 'nb_have_design' , 'yes' );
            }
        }
        $results = 'Update successful!';
        $response = rest_ensure_response( $results );
        return $response;
        // end
    }

    // function API
    public function getListUsers( $request = array() ) {
        global $wpdb;
        $posts_per_page = isset( $request['per_page'] ) ? (int) $request['per_page'] : 25;
        $page           = isset( $request['page'] ) ? (int) $request['page'] : 1;
        $user_id        = isset( $request['user_id'] ) ? $request['user_id'] : '';
        $email          = isset( $request['email'] ) ? $request['email'] : '';
        $name           = isset( $request['name'] ) ? $request['name'] : '';
        $offset         = ($page * $posts_per_page) - $posts_per_page;
        $query_first    = "SELECT ID FROM wp_users AS u INNER JOIN wp_usermeta AS mt1 ON ( u.ID = mt1.user_id ) ";
        //custom update have_design update database
        // $_query = $query_first." GROUP BY u.ID ORDER BY u.ID ASC";
        // $_users          = $wpdb->get_results($_query);
        // foreach( $_users AS $user ){
        //     $user_id    = $user->ID;
        //     $item       = $this->getDataUser($user_id , $index);
        //     $save_4_lates = $this->get_my_designs($user_id)['total'];
        //     if( $item['nbu_total'] > 0 || $item['nbd_total'] > 0 || $save_4_lates > 0) {
        //         update_user_meta( $user_id , 'nb_have_design' , 'yes' );
        //     }
        // }
        // end
        $query_last     = "";
         if($user_id != '') {
            $query_last .= " AND u.ID LIKE '%${user_id}%'";
        }
        if($name != '') {
            $query_last .= " AND ( ( mt1.meta_key = 'nickname' AND mt1.meta_value LIKE '%${name}%' ) OR ( mt1.meta_key = 'billing_first_name' AND  mt1.meta_value LIKE '%${name}%' ) OR (  mt1.meta_key = 'billing_last_name' AND  mt1.meta_value LIKE '%${name}%' ) )";
        }
        if($email != '') {
            $query_last .= " AND u.user_email LIKE '%${email}%'";
        }
        $query_total = $query_first. ' ' .$query_last."AND ( mt1.meta_key = 'nb_have_design' AND  mt1.meta_value = 'yes' ) GROUP BY u.ID ORDER BY u.ID ASC";
        $query_last     .= "AND ( mt1.meta_key = 'nb_have_design' AND  mt1.meta_value = 'yes' ) GROUP BY u.ID ORDER BY u.ID ASC LIMIT ${offset}, ${posts_per_page}";
        $query = $query_first . ' ' . $query_last;
        $users          = $wpdb->get_results($query);
        $index          = 1;
        $user_info      = array();
        $item           = array();
        $items          = array();
        $total = count($wpdb->get_results($query_total));
        foreach( $users AS $user ){
            $user_id    = $user->ID;
            $item       = $this->getDataUser($user_id , $index);
            $item['num_save_4_late'] = $this->get_my_designs($user_id)['total'];
            $item['list_save_4_lates'] = $this->get_my_designs($user_id)['designs'];
            $items[]    = $item;
            $index++;
        }
        $results['user'] =  $items;
        $results['total'] = $total;
        $response = rest_ensure_response( $results );
        return $response;
    }
    public function get_my_designs( $user_id ){
        global $wpdb;
        $designs    = $wpdb->get_results( "SELECT folder,created_date  FROM {$wpdb->prefix}nbdesigner_mydesigns AS design WHERE user_id = {$user_id}  AND ( design.status != 'expired' AND design.status != 'expired-ns' ) " );
        $results['designs'] = $designs;
        $results['total'] = count($designs);
        return $results;
    }
    public function getOrderByUsersId( $request ) {
        $user_id        = isset( $request['user_id'] ) ? $request['user_id'] : 0;
        $orderList      = array();
        $orderList      = $this->_getOrderByUsersId($user_id);
        $response       = rest_ensure_response( $orderList );
        return $response;
    }
    public function getListFolders( $request ) {
        $order_id       = isset( $request['order_id'] ) ? $request['order_id'] : '';
        $folderDesign   = $this->getFolderDesignByOrderId($order_id);
        $response       = rest_ensure_response( $folderDesign );
        return $response;
    }
    public function removeFolderDesigns( $request ) {
        $user_id = isset( $request['user_id'] ) ? $request['user_id'] : 0;
        $only_user_id = isset( $request['only_user_id'] ) ? $request['only_user_id'] : 0;
        $order_id = $request['order_id'];
        $list_users = json_decode($request['list_users']);
        $nbd = json_decode($request['nbd']);
        $nbu = json_decode($request['nbu']);
        $save_4_lates = json_decode($request['save_4_lates']);
        $logs = array();
        $custom_remove = false;
        if( ( $user_id && $order_id ) || ( $user_id && count($save_4_lates) > 0 ) ) {
            $custom_remove = true;
        }
        if( count($list_users) > 0 ) {
            foreach($list_users as $item) {
                $_user_id = $item->user_id;
                $this->removeFolderByUser($_user_id);
            }  
            return rest_ensure_response( true );
        }
        if( $only_user_id > 0 ) {
             $this->removeFolderByUser($only_user_id);
             return rest_ensure_response( true );
        }
        if( count($nbu) > 0 ) {
            foreach ($nbu as $key => $item) {
                if( $custom_remove && ( !isset($item->selected) || ( isset($item->selected) && !$item->selected ) ) ) {
                    continue;
                }
                if( isset($item->value) && $item->value ) {
                    $log = array();
                    $item_key = $item->value;
                    $item_id = $item->key;
                    $nbu_path = NBDESIGNER_UPLOAD_DIR . '/' . $item_key;
                    wc_update_order_item_meta($item_id, '_nbu', '');
                    self::deleteDir($nbu_path);
                    $currentDate = strtotime("now") + 8*60*60;
                    $date_create = date( 'H:i d-m-Y' , $currentDate  );
                    $log = array(
                        'user_id'       => $user_id,
                        'date_create'   => $date_create,
                        'folder_name'   => $item_key,
                        'type'          => 'Upload',
                    );
                    $logs[] = $log;
                }   
            }               
        }
        if( count($nbd) > 0 ) {
            foreach ($nbd as $key => $item) {
                if( $custom_remove && ( !isset($item->selected) || ( isset($item->selected) && !$item->selected ) ) ) {
                    continue;
                }
                if( isset($item->value) && $item->value ) {
                    $log = array();
                    $item_key = $item->value;
                    $item_id = $item->key;
                    $nbd_path = NBDESIGNER_CUSTOMER_DIR . '/' . $item_key;
                    $removeFileUpload = $this->removeUploadByFolderDesign($item_key);
                    self::deleteDir($nbd_path);
                    wc_update_order_item_meta($item_id, '_nbd', '');
                    $currentDate = strtotime("now") + 8*60*60;
                    $date_create = date( 'H:i d-m-Y' , $currentDate  );
                    $log = array(
                        'user_id'       => $user_id,
                        'date_create'   => $date_create,
                        'folder_name'   => $item_key,
                        'type'          => 'Design',
                    );
                    $logs[] = $log;  
                }   
            }                       
        }
        if( count($save_4_lates) > 0 ) {
            global $wpdb;

            foreach ($save_4_lates as $key => $item) {
                if( $custom_remove && ( !isset($item->selected) || ( isset($item->selected) && !$item->selected ) ) ) {
                    continue;
                }
                if( isset($item->folder) && $item->folder ) {
                    $log = array();
                    $item_key   = $item->folder;
                    $nbd_path = NBDESIGNER_CUSTOMER_DIR . '/' . $item_key;
                    $removeFileUpload = $this->removeUploadByFolderDesign($item_key);
                    self::deleteDir($nbd_path);
                    $wpdb->delete("{$wpdb->prefix}nbdesigner_mydesigns", array('folder' => $item_key));
                    $currentDate = strtotime("now") + 8*60*60;
                    $date_create = date( 'H:i d-m-Y' , $currentDate  );
                    $log = array(
                        'user_id'       => $user_id,
                        'date_create'   => $date_create,
                        'folder_name'   => $item_key,
                        'type'          => 'Save for late',
                    );
                    $logs[] = $log;
                }
            }                    
        }
        if(isset($order_id)) {
            $this->send_email_remove($order_id);
        }
        if(isset($user_id)) {
            $this->send_email_remove( 0 , $user_id);
        }
        if(count($logs) > 0) {
            $currenLog = unserialize(get_user_meta( $user_id , '_log_delete_folder' , true ));
            if(is_array($currenLog)) {
                $currenLog = array_merge($currenLog , $logs);
            } else {
                $currenLog = $logs;
            }
            update_user_meta( $user_id , '_log_delete_folder' , serialize($currenLog)); 
        }
        $response = rest_ensure_response( true );
        return $response;
    }

    public function getLogByUserId( $request ) {
        $user_id = $request['id'];
        if( !isset($user_id) ) return false;
        $log = unserialize( get_user_meta($user_id , '_log_delete_folder' , true) ) ;
        $response       = rest_ensure_response( $log );
        return $response;
    }

    // function 
    public function removeFolderByUser( $user_id ) {
        $orderDetail   = $this->_getOrderByUsersId( $user_id , true );
        $logs = array();
        if( count($orderDetail['nbu_list']) > 0 ) {
            foreach ($orderDetail['nbu_list'] as $item) {
                $log        = array();
                $item_key   = $item['value'];
                $item_id    = $item['key'];
                $nbu_path   = NBDESIGNER_UPLOAD_DIR . '/' . $item_key;
                wc_update_order_item_meta($item_id, '_nbu', '');
                self::deleteDir($nbu_path);
                $currentDate = strtotime("now") + 8*60*60;
                $date_create = date( 'H:i d-m-Y' , $currentDate  );
                $log = array(
                    'user_id'       => $user_id,
                    'date_create'   => $date_create,
                    'folder_name'   => $item_key,
                    'type'          => 'Upload',
                );
                $logs[] = $log;
            } 
        } 
        if( count( $orderDetail['nbd_list'] ) > 0 ) {
            foreach ($orderDetail['nbd_list'] as $item) {
                $log        = array();
                $item_key   = $item['value'];
                $item_id    = $item['key'];
                $nbd_path   = NBDESIGNER_CUSTOMER_DIR . '/' . $item_key;
                $removeFileUpload = $this->removeUploadByFolderDesign($item_key);
                self::deleteDir($nbd_path);
                wc_update_order_item_meta($item_id, '_nbd', '');
                $currentDate = strtotime("now") + 8*60*60;
                $date_create = date( 'H:i d-m-Y' , $currentDate  );
                $log = array(
                    'user_id'       => $user_id,
                    'date_create'   => $date_create,
                    'folder_name'   => $item_key,
                    'type'          => 'Design',
                );
                $logs[] = $log;
            } 
        } 
        $save_4_lates = $this->get_my_designs($user_id)['designs'];
        if( count( $save_4_lates ) > 0 ) {
            global $wpdb;
            foreach ($save_4_lates as $item) {
                $log        = array();
                $item_key   = $item->folder;
                $nbd_path   = NBDESIGNER_CUSTOMER_DIR . '/' . $item_key;
                $removeFileUpload = $this->removeUploadByFolderDesign($item_key);
                self::deleteDir($nbd_path);
                $wpdb->delete("{$wpdb->prefix}nbdesigner_mydesigns", array('folder' => $item_key));
                $currentDate = strtotime("now") + 8*60*60;
                $date_create = date( 'H:i d-m-Y' , $currentDate  );
                $log = array(
                    'user_id'       => $user_id,
                    'date_create'   => $date_create,
                    'folder_name'   => $item_key,
                    'type'          => 'Save for late',
                );
                $logs[] = $log;
            } 
        } 
        if(isset($user_id)) {
            $this->send_email_remove( 0 , $user_id);
        }
        if(count($logs) > 0) {
            $currenLog = unserialize(get_user_meta( $user_id , '_log_delete_folder' , true ));
            if(is_array($currenLog)) {
                $currenLog = array_merge($currenLog , $logs);
            } else {
                $currenLog = $logs;
            }
            update_user_meta( $user_id , '_log_delete_folder' , serialize($currenLog)); 
        }
        update_user_meta( $user_id , '_log_delete_folder' , serialize($currenLog) );
    }
    public function getTotalUser() {
        global $wpdb;
        $query = "SELECT * FROM {$wpdb->prefix}users ";
        $total = $wpdb->get_var("SELECT COUNT(1) FROM (${query}) AS combined_table");
        return (int) $total;
    }
    public function usersDetail($user_id) {
        $results = array();
        if($user_id) {
            $users = get_user_by( 'id' , $user_id);
            if($users) {
                 $results = array(
                    'name'      => $users->display_name ? $users->display_name : $users->first_name  . ' ' .$users->last_name ,
                    'email'     => $users->user_email,
                );
            }
        }
        return $results;
    }
    public function getDataUser( $user_id , $index = 1){
        if( !isset($user_id) ) { return false; }
        $user_info      = $this->usersDetail($user_id);
        $orderDetail   = $this->_getOrderByUsersId( $user_id , true );
        $item           = array(
            'index'     => $index,
            'user_id'   => $user_id,
            'name'      => $user_info['name'],
            'email'     => $user_info['email'],
            'nbu_total' => $orderDetail['nbu_total'],
            'nbd_total' => $orderDetail['nbd_total'],
            'nbu_list'  => $orderDetail['nbu_list'],
            'nbd_list'  => $orderDetail['nbd_list'],
        );
        return  $item;
    }
   
    public function getFolderDesignByOrderId( $order_id , $type = 'folder' ) {
        if( !isset($order_id) ) return false;
        $order  = wc_get_order($order_id);
        if($order) {
            $order_items    = $order->get_items('line_item');
            $nbd_items      = array();
            $nbu_items      = array();
            $nbd_item       = array();
            $nbu_item      = array();
            if(isset($order_items)) {
                foreach ( $order_items as $item_id => $item ) {
                    $nbd_item['key']  = $item_id;                       
                    $nbd_item['value']    = wc_get_order_item_meta($item_id , '_nbd');
                    $nbu_item['key']  = $item_id;                       
                    $nbu_item['value']    = wc_get_order_item_meta($item_id , '_nbu');                       
                    if( $nbd_item['value'] ) $nbd_items[] = $nbd_item;                                        
                    if( $nbu_item['value'] ) $nbu_items[] = $nbu_item;                                        
                }     
            }
            $results = array();
            $results['nbd_item'] = $nbd_items;
            $results['nbu_item'] = $nbu_items;
            return $results;
        } else {
            return false;
        }
    }
    public function _getOrderByUsersId( $user_id , $total_folder = false ) {
        if( !isset($user_id) ) { return false; }
        $order_ids = array();
        $customer_orders = get_posts( array(
            'numberposts' => -1,
            'meta_key'    => '_customer_user',
            'meta_value'  => $user_id,
            'post_type'   => 'shop_order',
            'post_status' => array_keys( wc_get_order_statuses() ),
        ) );
        $order      = array();
        $orderList  = array();
        $index      = 1; 
        $nbu_total  = 0;
        $nbd_total  = 0;
        $nbu_list   = array();
        $nbd_list   = array();
        foreach ($customer_orders as $customer_order) {
            $folderDesign  = $this->getFolderDesignByOrderId($customer_order->ID);
            $nbu_total    += count($folderDesign['nbu_item']);
            $nbd_total    += count($folderDesign['nbd_item']);
            $nbu_list      = array_merge($nbu_list, $folderDesign['nbu_item']);
            $nbd_list      = array_merge($nbd_list, $folderDesign['nbd_item']);
            $order         = array(
                'index'         => $index,
                'order_id'      => $customer_order->ID,
                'date_create'   => $customer_order->post_date,
                'nbu_count'     => count($folderDesign['nbu_item']),
                'nbd_count'     => count($folderDesign['nbd_item']),
                'nbu_items'     => $folderDesign['nbu_item'],
                'nbd_items'     => $folderDesign['nbd_item'],
                'save_4_lates'  => array(),
            );
            if( $order['nbu_count'] > 0 || $order['nbd_count'] > 0 ) {
                 $orderList[] = $order;
                $index++;
            }
        }
        if( count( $this->get_my_designs($user_id)['designs'] ) > 0 ) {
            $orderList[] = array(
                'index'         => $index,
                'order_id'      => '',
                'date_create'   => 'save_4_late',
                'nbu_count'     => 0,
                'nbd_count'     => 0,
                'nbu_items'     => array(),
                'nbd_items'     => array(),
                'save_4_lates'   => $this->get_my_designs($user_id)['designs'],
            );
        }
        $results = array();
        if( $total_folder ) {
            $results['nbu_total'] = $nbu_total;
            $results['nbd_total'] = $nbd_total;
            $results['nbu_list']  = $nbu_list;
            $results['nbd_list']  = $nbd_list;
            return $results;
        }
        return $orderList;
    }
    public function removeUploadByFolderDesign($nbd_key = '') {
        $flag = false;
        if( $nbd_key != '' ) {
            $design_content = json_decode( file_get_contents(NBDESIGNER_CUSTOMER_DIR. '/'.$nbd_key.'/design.json') , true );
            if( is_array( $design_content ) ) {
                foreach( $design_content as $content ) {
                    $object = $content['objects'];
                    if( is_array( $object ) ) {
                        foreach( $object as $obj ) {
                            $src_upload = isset( $obj['src'] ) ? $obj['src'] : '';
                            $src_pdf = isset( $obj['origin_pdf'] ) ? $obj['origin_pdf'] : '';
                            $origin_url = isset( $obj['origin_url'] ) ? $obj['origin_url'] : '';
                            if( $src_upload ) {
                                $path_upload = NBDESIGNER_TEMP_DIR. str_replace( 'temp' , '' , strchr( $src_upload , '/temp/') );
                                if( file_exists($path_upload)) {
                                    unlink($path_upload);
                                }
                            } 
                            if( $src_pdf ) {
                                $path_pdf = NBDESIGNER_TEMP_DIR. $src_pdf;
                                if( file_exists($path_pdf)) {
                                    unlink($path_pdf);
                                }
                            }
                            if( $origin_url ) {
                                $path_origin = NBDESIGNER_TEMP_DIR. str_replace( 'temp' , '' , strchr( $origin_url , '/temp/') );
                                if( file_exists($path_origin)) {
                                    unlink($path_origin);
                                    
                                }
                            }
                        }
                    }
                }
            }
            $flag = true;
        }
        return $flag;
    }
    public static function deleteDir($dirPath) {
        if (! is_dir($dirPath)) {
            return 'Directory could not be found';
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }
    public function send_email_remove($order_id = 0 , $user_id = 0 ) {
        $subject = "Welcome to Charmingprint!";
        if ($order_id != 0) {
            $order      = wc_get_order($order_id);
            $order_data = $order->get_data();
            $to         = $order_data['billing']['email'];
            $subject    = "Delete folder design #".$order_id;
            $temp_content = 'All folders design of order  ' .$order_id. ' has been deleted';
        }
        if ($user_id != 0) {
            $user =  get_user_by( 'id' , $user_id);
            $to = $user->user_email;
            $subject = "Delete folder design of user #".$user->ID;
            $temp_content = 'All folders design of user ' .$user->ID. ' has been deleted';
        }
        $content_email  =  htmlspecialchars_decode(stripslashes( get_option('nb_content_email_delete_design' ,  true ) ) );
        if($content_email) {
            $temp_content = $content_email;
        }
        if ($temp_content != '') {
            $headers = array('Content-Type: text/html; charset=UTF-8');
            ob_start();
            echo $temp_content;
            $message = ob_get_contents();
            ob_end_clean();
            wp_mail($to, $subject, $message, $headers);
        }
    }
}

add_filter( 'woocommerce_rest_api_get_rest_namespaces', 'woo_custom_api' );

function woo_custom_api( $controllers ) {
    $controllers['wc/v3']['custom'] = 'WC_REST_Manager_Design_Controller';

    return $controllers;
}