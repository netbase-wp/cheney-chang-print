<?php
if ( ! defined( 'ABSPATH' ) ) exit;

if( !class_exists('NBManagerDesign') ){

    class NBManagerDesign {
        // public $time_save_for_late_reg = 0;
        // public $checkout_reg = 0;
        // public $checkout_non_reg = 0;
        // public $dropped_design = 0;
        protected static $instance;
        public static function instance() {
            if ( is_null( self::$instance ) ) {
                self::$instance = new self();
            }
            return self::$instance;
        }
        public function __construct() {

        }
        public function init(){
            $this->ajax_hook();

            $this->create_table_cron_job_mangage_design();

            add_action( 'admin_menu' , array( $this , 'add_admin_menu' ) );
            add_action( 'admin_enqueue_scripts', array( $this, 'nb_admin_enqueue_scripts' ), 10, 1 );

            //
            add_action( 'woocommerce_checkout_order_processed', array( $this, 'nbd_detail_order_custom' ) , 10 , 3 );

            add_action( 'after_nbd_save_customer_design', array( $this, 'nbd_save_customer_design' ) , 10 , 1 );
            add_action( 'after_save_for_late', array( $this, 'nbd_before_save_for_late' ) , 10 , 1 );

            // hook upload design
            add_action( 'nbu_upload_design_file', array( $this, 'nbu_upload_design_file' ) , 10 , 3);

            add_action( 'nb_after_upload_file_in_editor', array( $this, 'nb_after_upload_file_in_editor' ) , 10 , 2);

            // add field to know between  non-register and register;
            add_action( 'woocommerce_checkout_posted_data', array($this, 'nb_checkout_posted_data' ) , 10 , 1 );

            // add alert on the page my design
            add_action( 'nb_before_show_design', array( $this, 'nb_alert_design_expired' ) , 10 , 1 );

            //created Cron schedule
            add_filter( 'cron_schedules' , array( $this , 'myprefix_custom_cron_schedule' ) );
            if ( !wp_next_scheduled('nb_remove_design_expiration_date' ) ) { 
                wp_schedule_event( time(), 'every_one_hours', 'nb_remove_design_expiration_date' );
            }
            add_action( 'nb_remove_design_expiration_date', array( $this , 'nb_create_cron_job' ) );
        }
        public function ajax_hook() {
            add_action( 'wp_ajax_never_show_again', array( $this , 'never_show_again' ) );
            add_action( 'wp_ajax_nopriv_never_show_again', array( $this , 'never_show_again' ) );
        }
        public function add_admin_menu() {
            add_menu_page('NB Design Management', 'NB Design Management', 'manage_options', 'mycs/design_management', array( $this ,'mangageFilesDesign' ), 'dashicons-tickets', 8);
           // add_submenu_page('mycs/design_management', 'Manage Files Design', 'Manage Files Design' , 'manage_options', 'mycs/mangage_files_design', array( $this , 'mangageFilesDesign' ) );
            add_submenu_page('mycs/design_management', 'Settings', 'Settings' , 'manage_options', 'mycs/settings', array( $this , 'settingExpirationDate' ) );
        }
        public function nb_admin_enqueue_scripts() {
             wp_enqueue_style( 'custom-css', NB_CUSTOM_CSS_URL . 'custom.css', '' , '' );
        }
        public function designManagement() {

        }
        public function mangageFilesDesign() {
            ?>
            <input type="text" id="specialist-linking" value="<?php echo esc_attr($specialist_linking);?>" style="display: none">
            <input type="text" id="current-user-id" value="<?php echo esc_attr($user_id);?>" style="display: none">
            <link href="<?php echo NB_CUSTOM_PLUGIN_URL . 'vue-manager/css/app.css'; ?>" rel="stylesheet">
            <style>
                #hello-world .card {
                   max-width: 100%!important;
                }
            </style>
            <div id=app>
            </div>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
            <script type="text/javascript" src="<?php echo NB_CUSTOM_PLUGIN_URL .'vue-manager/js/manifest.js?t='.strtotime("now"); ?>"></script>
            <script type="text/javascript" src="<?php echo NB_CUSTOM_PLUGIN_URL .'vue-manager/js/vendor.js?t='.strtotime("now"); ?>"></script>
            <script type="text/javascript" src="<?php echo NB_CUSTOM_PLUGIN_URL .'vue-manager/js/app.js?t='.strtotime("now"); ?>"></script>
            <?php
        }
        public function settingExpirationDate() {
            $exp_date               = unserialize( get_option('nb_exp_date') );
            $content_email          = get_option('nb_content_email_delete_design' ,  true );
            $time_save_for_late_reg = 0;
            $checkout_reg           = 0;
            $dropped_design         = 0;
            $checkout_non_reg       = 0;
            $nb_expiry_time         = 0;
            $unit                   = 's';
            if( $exp_date && is_array($exp_date) ) {
                $time_save_for_late_reg  = isset($exp_date['time_save_for_late_reg']) ? $exp_date['time_save_for_late_reg'] : 0;
                $checkout_reg            = isset($exp_date['checkout_reg']) ? $exp_date['checkout_reg'] : 0;
                $checkout_non_reg        = isset($exp_date['checkout_non_reg']) ? $exp_date['checkout_non_reg'] : 0;
                $dropped_design          = isset($exp_date['dropped_design']) ? $exp_date['dropped_design'] : 0;
                $nb_expiry_time          = isset($exp_date['nb_expiry_time']) ? $exp_date['nb_expiry_time'] : 0;
                $unit                    = isset($exp_date['unit']) ? $exp_date['unit'] : 's';
            }
            if( isset($_POST['_action']) && $_POST['_action'] == 'submit' ) {
                $time_save_for_late_reg         = isset($_POST['nb_expiration_date_registered_save_for_later']) ? (int) $_POST['nb_expiration_date_registered_save_for_later'] : 0;
                $checkout_reg                   = isset($_POST['nb_expiration_date_registered_checkout']) ? (int) $_POST['nb_expiration_date_registered_checkout'] : 0;
                $checkout_non_reg               = isset($_POST['nb_expiration_date_non_registered_checkout']) ? (int) $_POST['nb_expiration_date_non_registered_checkout'] : 0;
                $dropped_design                 = isset($_POST['nb_expiration_date_both_dropped']) ? (int) $_POST['nb_expiration_date_both_dropped'] : 0;
                $nb_expiry_time                 = isset($_POST['nb_expiry_time']) ? (int) $_POST['nb_expiry_time'] : 0;
                $unit                           = isset($_POST['nb_expiration_unit']) ? $_POST['nb_expiration_unit'] : 's';
                $content_email                  = isset($_POST['nb_content_email_delete_design']) ? $_POST['nb_content_email_delete_design'] : '';
                $exp_date = array(
                    'time_save_for_late_reg'    => $time_save_for_late_reg,
                    'checkout_reg'              => $checkout_reg,
                    'checkout_non_reg'          => $checkout_non_reg,
                    'dropped_design'            => $dropped_design,
                    'nb_expiry_time'            => $nb_expiry_time,
                    'unit'                      => $unit,
                );
                update_option('nb_exp_date' , serialize($exp_date) );
                update_option('nb_content_email_delete_design' , htmlspecialchars(stripslashes( $content_email) ));
                ?>
                <div id="message" class="updated inline"><p><strong>Your settings have been saved.</strong></p></div>
                <?php
            }
            ?>
            <style type="text/css">
                #nb-expiration-date div.description {
                    color: #909090;
                    font-style: italic;
                }
            </style>
            <div id="nb-expiration-date">
                <h1 class="title">Expiration Date Save Design</h1>
                <form method="post" id="nb-form-expiration-date" action="" enctype="multipart/form-data">
                    <table class="form-table table table-striped">
                        <tbody>
                            <tr valign="top">
                                <th class="titledesc">
                                    <label>Content email: <span class="nb-help-tip"></span></label>
                                </th>
                                <td>
                                    <?php 
                                        $content = htmlspecialchars_decode(stripslashes($content_email));
                                        $editor_id = 'nb_content_email_delete_design';
                                        $settings =   array(
                                            'textarea_name' => $editor_id,
                                            'textarea_rows' => 10,
                                            'tinymce'       => array(
                                                'theme_advanced_buttons1' => 'bold,italic,strikethrough,separator,bullist,numlist,separator,blockquote,separator,justifyleft,justifycenter,justifyright,separator,link,unlink,separator,undo,redo,separator',
                                                'theme_advanced_buttons2' => '',
                                            ),
                                        );
                                        wp_editor( $content, $editor_id, $settings );
                                    ?>
                                    <div class="description">Email sent to customer when design is deleted.</div>
                                </td>
                            </tr>
                            <tr valign="top">
                                <th class="titledesc">
                                    <label>Unit: <span class="nb-help-tip"></span></label>
                                </th>
                                <td>
                                    <ul>
                                        <li>
                                            <!-- <label><input name="nb_expiration_date_registered_save_for_later" value="<?php echo esc_attr($time_save_for_late_reg); ?>" type="number" style="" class=""> Save for later</label> -->
                                            <select name="nb_expiration_unit">
                                                <option <?php if($unit == 's') { echo 'selected';} ?> value="s">Second</option>
                                                <option <?php if($unit == 'm') { echo 'selected';} ?> value="m">Minute</option>
                                                <option <?php if($unit == 'h') { echo 'selected';} ?> value="h">Hour</option>
                                                <option <?php if($unit == 'd') { echo 'selected';} ?> value="d">Day</option>
                                            </select>
                                        </li>
                                    </ul>
                                    <div class="description">Unit of time used to setup expiration time.</div>
                                </td>
                            </tr>
                            <tr valign="top">
                                <th class="titledesc">
                                    <label>Expiry: <span class="nb-help-tip"></span></label>
                                </th>
                                <td>
                                    <ul>
                                        <li>
                                            <input name="nb_expiry_time" value="<?php echo esc_attr($nb_expiry_time); ?>" type="number" style="" class="">
                                        </li>
                                    </ul>
                                    <div class="description">Setup the time to show the warning in My design page prior to time of deletion.</div>
                                </td>
                            </tr>
                            <tr valign="top">
                                <th class="titledesc">
                                    <label>With registered customer: <span class="nb-help-tip"></span></label>
                                </th>
                                <td>
                                    <ul>
                                        <li>
                                            <label><input name="nb_expiration_date_registered_save_for_later" value="<?php echo esc_attr($time_save_for_late_reg); ?>" type="number" style="" class=""> Save for later</label>
                                            <div class="description">Setup the expiration time of saved design (save for later) in My design of registered customer.</div>
                                        </li>
                                        <li>
                                            <label><input name="nb_expiration_date_registered_checkout" value="<?php echo esc_attr($checkout_reg); ?>" type="number" style="" class=""> Checkout</label>
                                            <div class="description">Setup the expiration time of design saved after checkout (in Order) of registered customer.</div>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            <tr valign="top">
                                <th class="titledesc">
                                    <label>With non-registered customer: <span class="nb-help-tip"></span></label>
                                </th>
                                <td>
                                    <ul>
                                        <li>
                                            <label><input name="nb_expiration_date_non_registered_checkout" value="<?php echo esc_attr($checkout_non_reg); ?>" type="number" style="" class=""> Checkout</label>
                                        </li>
                                    </ul>
                                    <div class="description">Setup the expiration time of design saved after checkout (in Order) of non-registered customer.</div>
                                </td>
                            </tr>
                            <tr valign="top">
                                <th class="titledesc">
                                    <label>For both registered and non-registered: <span class="nb-help-tip"></span></label>
                                </th>
                                <td>
                                    <ul>
                                        <li>
                                            <label><input name="nb_expiration_date_both_dropped" value="<?php echo esc_attr($dropped_design); ?>" type="number" style="" class=""> Dropped Design, Uploaded Design and upload in editor</label>
                                        </li>
                                    
                                    </ul>
                                    <div class="description">Setup expiration time of design, uploaded design file and image upload inside the editor that is dropped (not checkout to order).</div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <p class="submit">
                        <button name="save" class="button-primary woocommerce-save-button" type="submit" value="Save changes">Save changes</button>
                        <input type="hidden" id="_action" name="_action" value="submit">
                    </p>
                </form>
            </div>
            <?php
        }
        public function create_table_cron_job_mangage_design(){
            global $wpdb;
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            $table_name = $wpdb->prefix."nb_mangage_design_files";

            $query = $wpdb->prepare( 'SHOW TABLES LIKE %s', $wpdb->esc_like( $table_name ) );

            if ( ! $wpdb->get_var( $query ) == $table_name ) {
                $collate = '';
                if ( $wpdb->has_cap( 'collation' ) ) {
                    $collate = $wpdb->get_charset_collate();
                } 
                $tables =  "
                    CREATE TABLE {$wpdb->prefix}nb_mangage_design_files ( 
                     id bigint(20) unsigned NOT NULL auto_increment,
                     order_id BIGINT(20) NOT NULL,
                     user_id  BIGINT(20) NOT NULL,   
                     created datetime NOT NULL default '0000-00-00 00:00:00',
                     str_date varchar(255) NULL,
                     email varchar(255) NULL,
                     nbu varchar(255) NULL,
                     nbd varchar(255) NULL,
                     type varchar(255) NULL,
                     link_uploads varchar(255) NULL,
                     file_name varchar(255) NULL,
                     PRIMARY KEY  (id)
                    ) $collate; 
                ";
                @dbDelta($tables);
                return 'Bảng đã được tạo';
            } else {
                return 'Bảng đã tồn tại';
            }
        }
        public function get_all_options() {
            global $wpdb;
            $result = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix."nb_mangage_design_files" , ARRAY_A );
            return $result;
        }
        public function add_option($order_id = '' , $args , $user_id = '') {
            global $wpdb;
            $date_created   = '';
            $email          = '';
            $nbu            = isset( $args['nbu'] ) ? $args['nbu'] : '';
            $nbd            = isset( $args['nbd'] ) ? $args['nbd'] : '';
            $params         = array();
            $params = array(
                'order_id'  => $order_id,
                'user_id'   => $user_id,
                'created'   => date( 'Y-m-d H:i:s' , strtotime('now') + 8*60*60),
                'str_date'  => strtotime('now') + 8*60*60,
                'email'     => '',
                'nbu'       => $nbu,
                'nbd'       => $nbd,
                'type'      => $args['type'],    
                'link_uploads'      => isset($args['link_uploads']) ? $args['link_uploads'] : '',    
                'file_name'      => isset($args['file_name']) ? $args['file_name'] : '',    
            );
            if($order_id) {
                $order      = wc_get_order($order_id);
                if($order) {
                    $user_id        = $order->get_user_id();
                    $email          = get_user_meta( $user_id )['billing_email'][0];
                }
                $params['email'] = $email; 
            }  
            if( $user_id ) {
                $params['email']    = $args['email'];
            }
            
            $wpdb->insert( $wpdb->prefix.'nb_mangage_design_files', $params );
        }
        public function get_option_by($key , $value) {
            global $wpdb;
            $sql = "SELECT * FROM ".$wpdb->prefix."nb_mangage_design_files AS df WHERE df.".$key." = '".$value."'";
            $result = $wpdb->get_results($sql , ARRAY_A);
            return $result;
        }
        public function isset_in_table($key , $value) {
            global $wpdb;
            $sql = "SELECT * FROM ".$wpdb->prefix."nb_mangage_design_files AS df WHERE df.".$key." = '".$value."'";
            $result = $wpdb->get_results($sql , ARRAY_A);
            if( count($result) > 0 ) {
                return true;
            }
            return false;
        }
        public function update_option($where = array(), $data = array()) {
            global $wpdb;
            return $wpdb->update("{$wpdb->prefix}nb_mangage_design_files", $data , $where );
        }
        public function delete_row($where = array()) {
            global $wpdb;
            return $wpdb->delete("{$wpdb->prefix}nb_mangage_design_files", $where );
        }
        public function get_file_upload_design($nbd_key) {

        }
        public function myprefix_custom_cron_schedule( $schedules ) {
            $schedules['every_one_hours'] = array(
                'interval' => 300, // Every 5 minutes
                'display'  => __( 'NB Every 5 minutes' ),
            );
            return $schedules;
        }
        public function nb_create_cron_job() { 
            $items      = $this->get_all_options();
            $exp_date   = unserialize( get_option('nb_exp_date') );
            $time_save_for_late_reg  = isset($exp_date['time_save_for_late_reg']) ? $exp_date['time_save_for_late_reg'] : 168;
            $checkout_reg            = isset($exp_date['checkout_reg']) ? $exp_date['checkout_reg'] : 168;
            $checkout_non_reg        = isset($exp_date['checkout_non_reg']) ? $exp_date['checkout_non_reg'] : 168;
            $dropped_design          = isset($exp_date['dropped_design']) ? $exp_date['dropped_design'] : 168;
            $warning_time            = isset($exp_date['nb_expiry_time']) ? $exp_date['nb_expiry_time'] : 168;
            $unit_name               = isset($exp_date['unit']) ? $exp_date['unit'] : 'h';
            switch ($unit_name) {
                case 's':
                    $unit = 1;
                    break;
                case 'm':
                    $unit = 60;
                    break;
                case 'h':
                    $unit = 3600;
                    break;
                case 'd':
                    $unit = 86400;
                    break;
                default:
                    $unit = 3600;
                    break;
            }

            foreach ($items as $item) {
                $id             = $item['id'];
                $type           = $item['type'];
                $time_created   = intval( $item['str_date']);
                $order_id       = $item['order_id'];
                $user_id        = $item['user_id'];
                $email          = $item['email'];
                $time_now       = strtotime('now') + 8*3600;
                $nbu            = explode(',', $item['nbu']); 
                $nbd            = explode(',', $item['nbd']);
                $link_uploads   =  $item['link_uploads'];
                $action         = '';
                switch ($type) {
                    case 'registered':
                        if($time_now - $time_created >= $checkout_reg * $unit) {
                            $action = 'expired';
                        }
                        break;
                    case 'non_registered':
                        if($time_now - $time_created >= $checkout_non_reg * $unit) {
                            $action = 'expired';
                        } 
                        break;
                    case 'dropped_design':
                        if($time_now - $time_created >= $dropped_design * $unit) {
                            $action = 'expired';
                        } 
                        break;
                    case 'save_for_late':
                        if($time_now - $time_created >= $time_save_for_late_reg * $unit) {
                            $action = 'expired';
                        } else {
                            if( $time_now - $time_created > ($time_save_for_late_reg  - $warning_time) * $unit ) {
                                $action = 'warning';
                            }
                        }
                        break;
                    default:
                        # code...
                        break;
                }
                if( $action ) {
                    $this->remove_folder_and_send_email ( $id , $nbu  , $nbd , $order_id , $email , $action , $link_uploads);
                }

            }
        }
        public function remove_file_upload_design( $item_key = '' ) {
            if( !$item_key ) return;
            $design_content = json_decode( file_get_contents(NBDESIGNER_CUSTOMER_DIR. '/'.$item_key.'/design.json') , true );
            if( is_array( $design_content ) ) {
                foreach( $design_content as $content ) {
                    $object = $content['objects'];
                    if( is_array( $object ) ) {
                        foreach( $object as $obj ) {
                            $src_upload = isset( $obj['src'] ) ? $obj['src'] : '';
                            $src_pdf = isset( $obj['origin_pdf'] ) ? $obj['origin_pdf'] : '';
                            $origin_url = isset( $obj['origin_url'] ) ? $obj['origin_url'] : '';
                            if( $src_upload ) {
                                $path_upload = NBDESIGNER_TEMP_DIR. str_replace( 'temp' , '' , strchr( $src_upload , '/temp/') );
                                if( file_exists($path_upload)) {
                                    unlink($path_upload);
                                }
                            } 
                            if( $src_pdf ) {
                                $path_pdf = NBDESIGNER_TEMP_DIR. $src_pdf;
                                if( file_exists($path_pdf)) {
                                    unlink($path_pdf);
                                }
                            }
                            if( $origin_url ) {
                                $path_origin = NBDESIGNER_TEMP_DIR. str_replace( 'temp' , '' , strchr( $origin_url , '/temp/') );
                                if( file_exists($path_origin)) {
                                    unlink($path_origin);
                                }
                            }
                        }
                    }
                }
            }
        }
        public function remove_folder_and_send_email ( $id , $nbu = array() , $nbd = array() , $order_id = '' , $email = '' , $action = '' , $link_uploads = '' ) {
            $sentMail = false;
            if( count($nbu) > 0 ) {
                foreach ($nbu as $item_key) {
                    if( $item_key && $action == 'expired' ) {
                        if( self::remove_folder( NBDESIGNER_UPLOAD_DIR . '/' .$item_key ) ) { 
                            $sentMail = true;
                        }
                        self::remove_folder( NBDESIGNER_UPLOAD_DIR . '/' .$item_key. '_preview' );
                    }
                }
            } 
            if( count($nbd) > 0 ) {
                foreach ($nbd as $item_key) {
                    if( $item_key  && $action == 'expired' ) {
                        $this->remove_file_upload_design($item_key); // remove file upload before remove folder
                        if( self::remove_folder( NBDESIGNER_CUSTOMER_DIR . '/' . $item_key ) ) { 
                            $sentMail = true;
                            // $this->nbd_delete_my_design($item_key);
                            $this->nbd_update_status_my_design($item_key , 'expired');
                        }
                        self::remove_folder( NBDESIGNER_CUSTOMER_DIR . '/' .$order_id. '_' . $item_key );
                        self::remove_folder( NBDESIGNER_CUSTOMER_DIR . '/' . $item_key. '_old' );
                    } else if( $item_key  && $action == 'warning' ) {
                        $this->nbd_update_status_my_design($item_key , 'warning');
                    }
                }
            } 
            if( $link_uploads  ) {
                $links = explode(',', $link_uploads);
                foreach ( $links as $link ) {
                    $dir = NBDESIGNER_TEMP_DIR. str_replace( '/temp' , '' , strchr( $link , '/temp/') );
                    if( file_exists($dir)) {
                        unlink($dir);
                    }
                }
                $this->delete_row(array( 'id' => $id));
            }
            if($sentMail) { 
                $this->delete_row(array( 'id' => $id));
                if($email) {
                    $this->send_email_remove( $email , $order_id);
                }
                $order = wc_get_order($order_id);
                if($order) {
                   foreach ( $order->get_items() as $item_id => $item ) {
                        wc_update_order_item_meta($item_id, '_nbd', '');
                        wc_update_order_item_meta($item_id, '_nbu', '');
                   }
                }  
            }
        }
        public static function remove_folder( $dirPath ) {
            if (! is_dir($dirPath)) {
                return 'Directory could not be found';
            }
            if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
                $dirPath .= '/';
            }
            $files = glob($dirPath . '*', GLOB_MARK);
            foreach ($files as $file) {
                if (is_dir($file)) {
                    self::remove_folder($file);
                } else {
                    unlink($file);
                }
            }
            rmdir($dirPath);
            return true;
        }
        public function send_email_remove( $email = '' , $order_id = 0 , $user_id = 0 , $temp_content = '') {
            $subject = "Welcome to Charmingprint!";
            if ($order_id ) {
                $order      = wc_get_order($order_id);
                $order_data = $order->get_data();
                $to         = $order_data['billing']['email'];
                $subject    = "Delete folder design #".$order_id;
                $temp_content = 'All folders design of order  ' .$order_id. ' has been deleted';
            }
            if ($user_id ) {
                $user =  get_user_by( 'id' , $user_id);
                $to = $user->user_email;
                $subject = "Delete folder design of user #".$user->ID;
                $temp_content = 'All folders design of user ' .$user->ID. ' has been deleted';
            }
            if( $email ) {
                $to = $email;
            }
            $content_email  =  htmlspecialchars_decode(stripslashes( get_option('nb_content_email_delete_design' ,  true ) ) );
            if($content_email) {
                $temp_content = $content_email;
            }
            if ($temp_content != '') {
                $headers = array('Content-Type: text/html; charset=UTF-8');
                ob_start();
                echo $temp_content;
                $message = ob_get_contents();
                ob_end_clean();
                wp_mail($to, $subject, $message, $headers);
            }
        }
        public function nbd_detail_order_custom($order_id, $posted_data, $order) {
            $type = 'registered';
            if( isset($posted_data['registered_guests']) && $posted_data['registered_guests'] ) {
                $type = 'non_registered';
            }
            $args = array();
            $args['type']   = $type;
            $args['nbd']    = '';
            $args['nbu']    = '';
            $args['email']  = $order->get_billing_email();
            $user_id        = $order->get_user_id();
            foreach ( $order->get_items() as $item_id => $item ) {
                $nbd_item_key = wc_get_order_item_meta($item_id, '_nbd');
                $nbu_item_key = wc_get_order_item_meta($item_id, '_nbu');
                if($nbd_item_key) {
                    $args['nbd'] .= ','.$nbd_item_key;
                    if( $this->isset_in_table('nbd' , $nbd_item_key) ) {
                        $this->delete_row( array( 'nbd' => $nbd_item_key) );
                    }
                }
                if($nbu_item_key) {
                    $args['nbu'] .= ','.$nbu_item_key;
                    if( $this->isset_in_table('nbu' , $nbu_item_key) ) {
                        $this->delete_row( array( 'nbu' => $nbu_item_key) );
                    }
                }
            }
            if( get_user_meta( $user_id , 'nb_have_design') != 'yes' ) {
                if( $nbd_item_key || $nbu_item_key ) {
                    update_user_meta( $user_id , 'nb_have_design' , 'yes' );
                }
            }
            $args['nbd'] = trim( $args['nbd'] , ',');
            $args['nbu'] = trim( $args['nbu'] , ',');
            $this->add_option( $order_id , $args , $user_id );
        } 
        // my design
        public function nbd_delete_my_design($item_key){
            global $wpdb;
            $re = $wpdb->delete("{$wpdb->prefix}nbdesigner_mydesigns", array('folder' => $item_key));
        }
        public function nbd_add_column_my_design(){
            global $wpdb;
            $row = $wpdb->get_results(  "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS
            WHERE table_name = '{$wpdb->prefix}nbdesigner_mydesigns' AND column_name = 'status'"  );
            if(empty($row)){
               $wpdb->query("ALTER TABLE {$wpdb->prefix}nbdesigner_mydesigns ADD status varchar(255) NOT NULL DEFAULT '' ");
            } 
            // else {
            //     $wpdb->query("ALTER TABLE {$wpdb->prefix}nbdesigner_mydesigns DROP COLUMN state ");
            // }
        }
        public function nbd_update_status_my_design($value , $status , $key = ''){
            global $wpdb;
            $this->nbd_add_column_my_design();
            $key =  !$key ? 'folder' : $key;
            if( $value ) {
                $re = $wpdb->update("{$wpdb->prefix}nbdesigner_mydesigns", array( 'status' => $status ) , array( $key => $value) );
                return $re;
            }
            return false;
        }

        public function nb_checkout_posted_data($data) {
            $customer_id = get_current_user_id();
            if( !isset($customer_id) || !$customer_id ) {
                if( 'yes' === get_option( 'woocommerce_enable_signup_and_login_from_checkout' ) ) {
                    $data['registered_guests'] = true;
                }
            }
            return $data;
        }
        public function nbd_save_customer_design($result) {
            if(isset($result['task']) && ( $result['task'] == 'create' || $result['task'] == 'edit' ) ) return; // custom charmingpront 22/12
            $user_id      = get_current_user_id();
            $args         = array();
            $args['type'] = 'dropped_design';
            if($user_id) {
                $args['email']   = get_user_meta( $user_id )['billing_email'][0];
            } 
            if( isset($result['folder'])) {
                $args['nbd']         = $result['folder'];
                $design_content = json_decode( file_get_contents(NBDESIGNER_CUSTOMER_DIR. '/'.$result['folder'].'/design.json') , true );
                if( is_array( $design_content ) ) {
                    foreach( $design_content as $content ) {
                        $object = $content['objects'];
                        if( is_array( $object ) ) {
                            foreach( $object as $obj ) {
                                $src = isset( $obj['src'] ) ? $obj['src'] : '';
                                $origin_url = isset( $obj['origin_url'] ) ? $obj['origin_url'] : '';
                                $src_upload = $origin_url ? $origin_url : $src;
                                if($src_upload) {
                                    $file_name = basename($src_upload, '.'.pathinfo($src_upload, PATHINFO_EXTENSION));
                                    if( $file_name ) {
                                        $this->delete_row( array( 'file_name' => $file_name));
                                    }
                                }
                            }
                        }
                    }
                }
                if( $this->isset_in_table('nbd' , $result['folder']) ) {
                    $this->delete_row( array( 'nbd' => $result['folder']) );
                }

                if($args['nbd']) {
                    $this->add_option('' , $args , $user_id);
                }
            }   
        }
        public function nbd_before_save_for_late($folder) {
            $user_id      = get_current_user_id();
            $args         = array();
            $args['type'] = 'save_for_late';
            $design_content = json_decode( file_get_contents(NBDESIGNER_CUSTOMER_DIR. '/'.$folder.'/design.json') , true );
            if( is_array( $design_content ) ) {
                foreach( $design_content as $content ) {
                    $object = $content['objects'];
                    if( is_array( $object ) ) {
                        foreach( $object as $obj ) {
                            $src_upload = isset( $obj['origin_url'] ) ? $obj['origin_url'] : $obj['src'];
                            if($src_upload) {
                                $file_name = basename($src_upload, '.'.pathinfo($src_upload, PATHINFO_EXTENSION));
                                if( $file_name ) {
                                    $this->delete_row( array( 'file_name' => $file_name));
                                }
                            }
                        }
                    }
                }
            }
            if($user_id) {
                if( get_user_meta( $user_id , 'nb_have_design') != 'yes' ) {
                    if( $nbd_item_key || $nbu_item_key ) {
                        update_user_meta( $user_id , 'nb_have_design' , 'yes' );
                    }
                }
                $args['date_created']   = date( 'Y-m-d H:i:s' , strtotime('now') + 8*60*60);
                $args['str_date']       = strtotime('now') + 8*60*60;
                $args['email']          = get_user_meta( $user_id )['billing_email'][0];
                $args['nbd']            = $folder;
                $this->add_option('' , $args , $user_id);
            }
        }
        public function nbu_upload_design_file( $nbu_item_key, $path, $result  ) {
            $user_id      = get_current_user_id();
            $type         = 'dropped_design';    
            $args = array();
            if($user_id) {
                $args['email']   = get_user_meta( $user_id )['billing_email'][0];
            }
            if($nbu_item_key) {
                $args['nbu']            = $nbu_item_key;
                $args['type']           = $type ;
            }
            $this->add_option('' , $args , $user_id);
        }
        public function nb_after_upload_file_in_editor( $res , $new_name  ) {
            $user_id      = get_current_user_id();
            $type         = 'dropped_design';    
            $args = array();
            $args['type'] = $type;
            if($user_id) {
                $args['email']   = get_user_meta( $user_id )['billing_email'][0];
            }
            $link_uploads = '';
            if( isset($res['src']) ) {
                $link_uploads = $res['src'];
            }
            if( isset($res['origin_pdf']) ) {
                $link_uploads .= ','.$res['origin_pdf'];
            }
            if( isset($res['origin_url']) ) {
                $link_uploads .= ','.$res['origin_url'];
            }
            if( $link_uploads ) {
                $link_uploads = ltrim( $link_uploads , ',');
            }
            $file_name = '';
            if( $new_name ) {
                $file_name = basename($new_name, '.'.pathinfo($new_name, PATHINFO_EXTENSION));
            }
            if( $file_name && $link_uploads ) {
                $args['link_uploads']    = $link_uploads;
                $args['file_name']       = $file_name ;
            }
            $this->add_option('' , $args , $user_id);
        }
        public function nb_alert_design_expired( $designs ) {
            $nbd_warning = array();
            $nbd_expired = array();
            $exp_date   = unserialize( get_option('nb_exp_date') );
            $warning_time            = $exp_date['nb_expiry_time'] ? $exp_date['nb_expiry_time'] : 168;
            $warning_time_name       = $warning_time . ' hour';
            $unit_name               = $exp_date['unit'] ? $exp_date['unit'] : 'h';
            switch ($unit_name) {
                case 's':
                    $unit = 1;
                    $warning_time_name = $warning_time . ' second';
                    break;
                case 'm':
                    $unit = 60;
                    $warning_time_name = $warning_time . ' minute';
                    break;
                case 'h':
                    $unit = 3600;
                    $warning_time_name = $warning_time . ' hour';
                    break;
                case 'd':
                    $unit = 86400;
                    $warning_time_name = $warning_time . ' day';
                    if($warning_time == 1) {
                        $warning_time_name = ' today';
                    }
                    break;
                default:
                    $unit = 3600;
                    break;
            }
            foreach ( $designs as $design ) {
                if( $design->status == 'warning' ) {
                    $nbd_warning[] = $design->id;
                }
                if( $design->status == 'expired' ) {
                    $nbd_expired[] = $design->id;
                }
            }
            ?>
            <style>
                .nb-alert-warning {
                    width: 100%;
                    height: auto;
                    border: 1px #f00 solid;
                    padding: 10px 20px;
                    background: rgb(248 16 16 / 10%);
                    border-radius: 5px;
                    margin-bottom: 10px;
                }
                .nb-alert-warning.warning {
                    border: 1px #ff6f00 solid;
                    background: rgb(255 134 0 / 20%);
                }
                .nb-alert-warning.expired {
                    border: 1px #f00 solid;
                    background: rgb(248 16 16 / 10%);
                }
                .nb-alert-warning .content-alert {
                    display: inline-block;
                }
                .never-show{
                    font-size: 12px;
                    font-weight: 400;
                    color: #000;
                }
                .nb-alert-warning input[type="checkbox"] {
                    margin-right: 10px;
                }
                .nb-alert-warning .design-number {
                    color: #f00;
                }
                .nb-alert-warning p {
                    color: #000;
                    display: inline-block;
                }
                .nb-alert-warning.hidden {
                    display: none;
                }
            </style>
            <?php
            if( count( $nbd_warning ) > 0 ) {
                ?>
                <div class="nb-alert-warning warning">
                    <p class="content-alert content-alert-warning">
                        <p>The following designs: </p><span class="design-number"><?php echo ' '.implode( ' , ' , $nbd_warning).' '; ?><span><p>  will expire after <?php echo $warning_time_name; ?>!</p>
                    </p>
                </div>
                <?php 
            }
            if( count( $nbd_expired ) > 0 ) {
                ?>
                <div class="nb-alert-warning expired">
                    <p class="content-alert content-alert-expired">
                        <p>The following designs: </p><span class="design-number"><?php echo ' '.implode( ' , ' , $nbd_expired).' '; ?><span><p> deleted!</p>
                    </p>
                    <div class="never-show">
                        <input class="checkbox" type="checkbox" data-design-id="<?php echo  implode( ',' , $nbd_expired); ?>"><span class="text">Never show again</span>
                    </div>
                </div>
                <script type="text/javascript">
                    var dataDesignId = jQuery('.never-show input.checkbox').data('design-id');
                    jQuery('.never-show input.checkbox').val(this.checked);
                    jQuery('.never-show input.checkbox').change( function() {
                        if(this.checked) {
                            var returnVal = confirm("Are you sure?");
                            if(returnVal) {
                                jQuery('.nb-alert-warning.expired').addClass('hidden');
                                jQuery.ajax({
                                    type : "post",
                                    dataType : "json",
                                    url : '<?php echo admin_url('admin-ajax.php');?>',
                                    data : {
                                        action: "never_show_again",
                                        data : dataDesignId,
                                    },
                                    context: this,
                                    success: function(response) {
                                        
                                    },
                                })  
                            }
                        }
                    })
                </script>
                <?php 
            }
        }
        public function never_show_again() {
            $data = $_POST['data'];
            if( isset( $data ) ) {
                $datas = explode(',', $data);
                if( count( $datas ) > 0 ) {
                    foreach( $datas as $id ) {
                        $this->nbd_update_status_my_design( $id , 'expired-ns' , 'id' );
                    }
                } 
            }
            return true;
        }
        //
    }
}

$nbd_manage = NBManagerDesign::instance();
$nbd_manage->init();