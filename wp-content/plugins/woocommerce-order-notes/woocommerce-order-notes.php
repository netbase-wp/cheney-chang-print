<?php
/**
 * Plugin Name: WooCommerce Order Notes
 * Plugin URI: http://www.actualityextensions.com
 * Description: Allows you to edit the order notes straight from the orders page and set a colour for the notes icon.
 * Version: 1.0.0
 * Author: Actuality Extensions
 * Author URI: http://www.actualityextensions.co.uk/
 * Tested up to: 4.1
 *
 * Copyright: (c) 2012-2015 Actuality Extensions
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * @package     WooCommerce-Orders-Notes
 * @author      ActualityExtensions
 * @category    Plugin
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

if (function_exists('is_multisite') && is_multisite()) {
    include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    if ( !is_plugin_active( 'woocommerce/woocommerce.php' ) )
        return;
}else{
    if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins'))))
        return; // Check if WooCommerce is active    
}

if (!class_exists('WooCommerceOrdersNotes')) {

    /**
     * Main WooCommerceOrdersNotes Class
     *
     * @class WooCommerceOrdersNotes
     * @version 1.9
     */
    class WooCommerceOrdersNotes {

        /**
         * @var string
         */
        public $version = '1.0.0';

        /**
         * @var WooCommerceOrdersNotes The single instance of the class
         *
         */
        protected static $_instance = null;
        /**
         * Main WooCommerceOrdersNotes Instance
         *
         * Ensures only one instance of WooCommerceOrdersNotes is loaded or can be loaded.
         *
         *
         * @static
         * @see WC_ON()
         * @return WooCommerceOrdersNotes - Main instance
         */
        public static function instance() {
            if ( is_null( self::$_instance ) ) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        /**
         * Cloning is forbidden.
         *
         *
         */
        public function __clone() {
            _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'woocommerce' ), '1.9' );
        }

        /**
         * Unserializing instances of this class is forbidden.
         *
         *
         */
        public function __wakeup() {
            _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'woocommerce' ), '1.9' );
        }

        /**
         * WooCommerceOrdersNotes Constructor.
         * @access public
         * @return WooCommerceOrdersNotes
         */

        public function __construct() {
            $this->includes();
            if(isset($_GET['post_type']) && $_GET['post_type'] == 'shop_order'){
                add_filter( 'manage_edit-shop_order_columns', array($this,'add_order_notes_column'), 20 );
                add_action( 'manage_shop_order_posts_custom_column', array($this,'set_order_notes_column_content'));
                add_action( 'admin_enqueue_scripts', array($this, 'enqueue_scripts') );
                add_action( 'admin_footer', array($this, 'admin_footer') );
                add_action( 'woocommerce_admin_order_actions_start', array($this, 'add_order_note_icon_style') );
            }

            add_filter('woocommerce_general_settings', array($this, 'add_settings_tab'));
        }

        function includes(){
            if (defined('DOING_AJAX')) {
                require_once( 'class-woocommerce-order-notes-ajax.php' );
            }
        }

        function add_settings_tab($settings){
            $settings[] = array( 'title' => __( 'Order Notes', 'woocommerce-order-notes' ), 'type' => 'title', 'id' => 'orders_notes' );
            $settings[] = array(
                'title'    => __( 'Default Order Notes', 'woocommerce-order-notes' ),
                'id'       => 'woocommerce_default_order_notes',
                'css'      => 'min-width:350px;',
                'default'  => 'customer',
                'type'     => 'select',
                'options'  => array(
                    'customer' => __( 'Customer', 'woocommerce-order-notes' ),
                    'private'  => __( 'Private', 'woocommerce-order-notes' )
                )
            );
            $settings[] = array( 'type' => 'sectionend', 'id' => 'orders_notes');
            return $settings;
        }

        /**
         * Enqueue admin CSS and JS dependencies
         */
        public function enqueue_scripts() {
            wp_enqueue_style( 'wp-color-picker' );

            wp_register_script( 'wc_order_notes', plugins_url( 'assets/wc_order_notes.js', __FILE__ ), array('jquery', 'wp-color-picker') );
            wp_enqueue_script( 'wc_order_notes' );
            $params = array(
                'ajax_url'            => admin_url('admin-ajax.php'),
                'security_get_notes'  => wp_create_nonce("get_notes"),
                'security_delete_order_note'  => wp_create_nonce("delete-order-note")
            );
            wp_localize_script( 'wc_order_notes', 'wc_order_notes', $params );

            wp_register_style( 'wc_order_notes', plugins_url( 'assets/wc_order_notes.css', __FILE__ ) );
            wp_enqueue_style( 'wc_order_notes' );
        }

        function admin_footer(){
            ?>
            <div id="WooCommerceOrdersNotesContent" tabindex="0">
                <div class="wc-backbone-modal">
                    <div class="wc-backbone-modal-content" style="margin-top: -105.5px;">
                        <section role="main" class="wc-backbone-modal-main">
                            <header class="wc-backbone-modal-header">
                                <a href="#" class="modal-close modal-close-link"><span class="close-icon"><span class="screen-reader-text"><?php  _e( 'Close', 'woocommerce-order-notes' ); ?></span></span></a>
                                <h1><?php  _e( 'Order Notes', 'woocommerce-order-notes' ); ?></h1>
                            </header>
                            <article style="overflow: visible; ">
                            </article>
                            <footer>
                                <div class="inner">
                                    <div id="wc_order_note_settings">
                                    </div>
                                </div>
                            </footer>
                        </section>
                    </div>
                </div>
                <div class="wc-backbone-modal-backdrop modal-close">&nbsp;</div>
            </div>
            <!-- <div id="WooCommerceOrdersNotesContent"></div>
            <div id="WooCommerceOrdersNotesOverlay"></div> -->
            <?php
        }

        function output_order_notes($order_id){
            $args = array(
                'post_id'   => $order_id,
                'approve'   => 'approve',
                'type'      => 'order_note'
            );

            remove_filter( 'comments_clauses', array( 'WC_Comments', 'exclude_order_comments' ), 10, 1 );

            $notes = get_comments( $args );

            add_filter( 'comments_clauses', array( 'WC_Comments', 'exclude_order_comments' ), 10, 1 );

            echo '<ul class="order_notes">';

            if ( $notes ) {

                foreach( $notes as $note ) {

                    $note_classes = get_comment_meta( $note->comment_ID, 'is_customer_note', true ) ? array( 'customer-note', 'note' ) : array( 'note' );

                    ?>
                    <li rel="<?php echo absint( $note->comment_ID ) ; ?>" class="<?php echo implode( ' ', $note_classes ); ?>">
                        <div class="note_content">
                            <?php echo wpautop( wptexturize( wp_kses_post( $note->comment_content ) ) ); ?>
                        </div>
                        <p class="meta">
                            <abbr class="exact-date" title="<?php echo $note->comment_date; ?>"><?php printf( __( 'added on %1$s at %2$s', 'woocommerce' ), date_i18n( wc_date_format(), strtotime( $note->comment_date ) ), date_i18n( wc_time_format(), strtotime( $note->comment_date ) ) ); ?></abbr>
                            <?php if ( $note->comment_author !== __( 'WooCommerce', 'woocommerce' ) ) printf( ' ' . __( 'by %s', 'woocommerce' ), $note->comment_author ); ?>
                            <a href="#" class="delete_note"><?php _e( 'Delete note', 'woocommerce' ); ?></a>
                        </p>
                    </li>
                    <?php
                }

            } else {
                echo '<li>' . __( 'There are no notes for this order yet.', 'woocommerce' ) . '</li>';
            }

            echo '</ul>';
            ?>
            <form class="add_note" id="add_note_form">
                <h4><?php _e( 'Add note', 'woocommerce' ); ?></h4>
                <p>
                    <textarea type="text" name="order_note" id="add_order_note" class="input-text" cols="20" rows="5"></textarea>
                </p>
                <p>
                    <?php $default = get_option('woocommerce_default_order_notes'); ?>
                    <select name="order_note_type" id="order_note_type">
                        <option value="customer" <?php selected('customer', $default, true); ?> ><?php _e( 'Customer note', 'woocommerce' ); ?></option>
                        <option value="" <?php selected('private', $default, true); ?>><?php _e( 'Private note', 'woocommerce' ); ?></option>
                    </select>
                    <button type="submit" class="add_note button button-primary"><?php _e( 'Add Note', 'woocommerce' ); ?></button>
                    <input type="hidden" value="<?php echo $order_id; ?>" name="order_id" id="add_order_note_order_id">
                    <input type="hidden" value="wc_order_notes_add_note" name="action">
                    <input type="hidden" value="<?php echo wp_create_nonce("add_note"); ?>" name="security">
                </p>
            </form>
            <form id="wc_order_note_settings_form">
                <?php
                $color = get_post_meta($order_id, 'note_settings_color', true);
                ?>
                <input type="text" name="note_settings_color" id="note_settings_color" value="<?php echo $color; ?>">
                <button type="submit" class="add_note button button-primary"><?php _e( 'Save Colour', 'woocommerce' ); ?></button>
                <input type="hidden" value="wc_order_notes_save_note_settings" name="action">
                <input type="hidden" value="<?php echo $order_id; ?>" name="note_settings_order_id" id="note_settings_order_id">
                <input type="hidden" value="<?php echo wp_create_nonce("save_note_settings"); ?>" name="security">
            </form>
            <?php
        }

        function add_order_note_icon_style($the_order){
            $color = get_post_meta($the_order->get_id(), 'note_settings_color', true);
            if(!empty($color)){
                ?>
                <style>
                    tr#post-<?php echo $the_order->get_id(); ?> td.order_notes span{color: <?php echo $color; ?>;}
                </style>
                <?php
            }
        }

        /****** Helper functions ******/

        /**
         * Get the plugin url.
         *
         * @return string
         */
        public function plugin_url() {
            return untrailingslashit( plugins_url( '/', __FILE__ ) );
        }

        /**
         * Get the plugin path.
         *
         * @return string
         */
        public function plugin_path() {
            return untrailingslashit( plugin_dir_path( __FILE__ ) );
        }



        /**
         * Adds 'Order Notes' column header to 'Orders' page immediately after 'Order Status' column.
         *
         * @param string[] $columns
         * @return string[] $new_columns
         */

        public function add_order_notes_column($columns){

            $new_columns = array();

            foreach ( $columns as $column_name => $column_info ) {

                $new_columns[ $column_name ] = $column_info;

                if ( 'order_status' === $column_name ) {
                    $new_columns['order_notes'] = '<span class="order-notes_head tips" data-tip="Order Notes">Order notes</span>';
                }
            }

            return $new_columns;
        }


        /**
         * Set 'Order Notes' column content to 'Orders' page.
         *
         * @param string[] $column name of column being displayed
         */
        public function set_order_notes_column_content($column){

            global $post;

            if ( 'order_notes' === $column ) {

                if ( $post->comment_count ) {

                    $latest_notes = wc_get_order_notes( array(
                        'order_id' => $post->ID,
                        'limit'    => 1,
                        'orderby'  => 'date_created_gmt',
                    ) );

                    $latest_note = current( $latest_notes );

                    if ( isset( $latest_note->content ) && 1 == $post->comment_count ) {
                        echo '<span class="note-on tips" data-tip="' . wc_sanitize_tooltip( $latest_note->content ) . '">' . __( 'Yes', 'woocommerce' ) . '</span>';
                    } elseif ( isset( $latest_note->content ) ) {
                        /* translators: %d: notes count */
                        echo '<span class="note-on tips" data-tip="' . wc_sanitize_tooltip( $latest_note->content . '<br/><small style="display:block">' . sprintf( _n( 'Plus %d other note', 'Plus %d other notes', ( $post->comment_count - 1 ), 'woocommerce' ), $post->comment_count - 1 ) . '</small>' ) . '">' . __( 'Yes', 'woocommerce' ) . '</span>';
                    } else {
                        /* translators: %d: notes count */
                        echo '<span class="note-on tips" data-tip="' . wc_sanitize_tooltip( sprintf( _n( '%d note', '%d notes', $post->comment_count, 'woocommerce' ), $post->comment_count ) ) . '">' . __( 'Yes', 'woocommerce' ) . '</span>';
                    }
                } else {
                    echo '<span class="na">&ndash;</span>';
                }
            }
        }


    }
    /**
     * Returns the main instance of WooCommerceOrdersNotes to prevent the need to use globals.
     *
     * @return WooCommerceOrdersNotes
     */
    function WC_ON() {
        return WooCommerceOrdersNotes::instance();
    }

    $GLOBALS['WooCommerceOrdersNotes'] = WC_ON();

}