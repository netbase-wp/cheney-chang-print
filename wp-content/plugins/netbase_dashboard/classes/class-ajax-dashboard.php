<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if( ! class_exists('NBD_Ajax') ) {
	class NBD_Ajax extends NBD_Singleton {

	    /**
	     * NPC_Register constructor.
	     *
	     * @since 0.1.0
	     */
	    public function __construct() {
			add_action( 'wp_ajax_refresh_selling', array($this, 'nb_refresh_selling') );
			add_action( 'rest_api_init', array($this, 'register_rest_route') );
	    }
		
	    public function register_rest_route() {

            register_rest_route(
                'nbdash/v1',
                '/logout',
                array(
                    'methods'  => 'POST',
                    'callback' => array( &$this, 'rest_logout_callback' ),
                )
            );
			
            register_rest_route(
                'nbdash/v1',
                '/cf7field',
                array(
                    'methods'  => 'POST',
                    'callback' => array( &$this, 'rest_cf7field_callback' ),
                )
            );
			
            register_rest_route(
                'nbdash/v1',
                '/read_notification',
                array(
                    'methods'  => 'POST',
                    'callback' => array( &$this, 'rest_read_notification_callback' ),
                )
            );
			
			register_rest_route(
				'nbdash/v1',
				'/product_sales',
				array(
					'methods'  => 'POST',
					'callback' => array( &$this, 'rest_product_sales_callback' ),
				)
			);

			register_rest_route(
				'nbdash/v1',
				'/live_search',
				array(
					'methods'  => 'POST',
					'callback' => array( &$this, 'rest_live_search_callback' ),
				)
			);
			
			register_rest_route(
				'nbdash/v1',
				'/product_order_status',
				array(
					'methods'  => 'POST',
					'callback' => array( &$this, 'rest_product_order_status_callback' ),
				)
			);

			register_rest_route(
				'nbdash/v1',
				'/product_order_filter',
				array(
					'methods'  => 'POST',
					'callback' => array( &$this, 'rest_product_order_filter_callback' ),
				)
			);
		}
		
		public function nb_refresh_selling() {
			global $post;

			/* Recent Orders */
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
			if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
				$sell_product_args = array(
					'post_type' => 'product',
					'meta_key' => 'total_sales',
					'orderby' => 'meta_value_num',
					'posts_per_page' => 5,
				);
				$query_seller = new WP_Query( $sell_product_args );
			}else {
				$query_seller = '';
			}

			if ( $query_seller->have_posts() ) {
				while ( $query_seller->have_posts() ) {
					$query_seller->the_post();
					$product = wc_get_product($post->ID);
					$thumbnail = wp_get_attachment_url( get_post_thumbnail_id( $product->get_id() ) );
					?>
					<li class="clearfix">
						<a href="<?php echo get_permalink($product->get_id());?>" class="selling-product-thumb" target="_blank">
							<img src="<?php echo $thumbnail;?>" />
						</a>
						
						<div class="selling-product-right">
							<div class="selling-product-detail">
								<h4><a href="<?php echo get_permalink($product->get_id());?>" title="<?php echo $product->get_title();?>" target="_blank"><?php echo $product->get_title();?></a></h4>
								<p><?php echo wp_trim_words( get_the_excerpt( $product->get_id() ), 8, '...' );?></p>
							</div>
							
							<span class="price"><?php echo $product->get_price_html();?></span>
						</div>
					</li>
					<?php }
				wp_reset_postdata();
			}
			die();
		}
		
		public function rest_product_order_filter_callback( WP_REST_Request $request ) {
			$json = array();
			$filter = $request->get_param( 'filter' );
			$status = $request->get_param( 'status' );
			$range = $request->get_param( 'range' );

			$sales_by_date  = new NB_Report_Order();

			if( ! empty($status) ) {
				$sales_by_date->order_status = array($status);
			}

			if( !empty($range) ) {
				$range_day = explode('-', $range);

				if( count($range_day) == 2) {
					$sales_by_date->start_date = strtotime($range_day[0]);
					$sales_by_date->end_date = strtotime($range_day[1]);
				}

			}else {
				if( $filter == 'this-week' ) {
					$sales_by_date->start_date = strtotime( 'monday this week' );
					$sales_by_date->end_date = strtotime( 'sunday this week' );
				}
				
				if( $filter == 'last-week' ) {
					$sales_by_date->start_date = strtotime( 'monday last week' );
					$sales_by_date->end_date = strtotime( 'sunday last week' );
				}
				
				if( $filter == 'this-month' ) {
					$sales_by_date->start_date = strtotime("first day of this month");
					$sales_by_date->end_date = strtotime("last day of this month");
				}
				
				if( $filter == 'last-month' ) {
					$sales_by_date->start_date = strtotime("first day of last month");
					$sales_by_date->end_date = strtotime("last day of last month");
				}
			}

			$sales_by_date->chart_groupby  = 'day';
			$sales_by_date->group_by_query = 'YEAR(posts.post_date), MONTH(posts.post_date), DAY(posts.post_date)';

			wp_send_json($sales_by_date->get_report_filter_data());
		}
		
		public function rest_product_order_status_callback( WP_REST_Request $request ) {
			$json = array();
			$filter = $request->get_param( 'filter' );
			$status = $request->get_param( 'status' );
			$range = $request->get_param( 'range' );

			$sales_by_date  = new NB_Report_Order();

			if( ! empty($status) ) {
				$sales_by_date->order_status = array($status);
			}

			if( !empty($range) ) {
				$range_day = explode('-', $range);

				if( count($range_day) == 2) {
					$sales_by_date->start_date = strtotime($range_day[0]);
					$sales_by_date->end_date = strtotime($range_day[1]);
				}

			}else {
				if( $filter == 'chartorder-this-week' ) {
					$sales_by_date->start_date = strtotime( 'monday this week' );
					$sales_by_date->end_date = strtotime( 'sunday this week' );
				}
				
				if( $filter == 'chartorder-last-week' ) {
					$sales_by_date->start_date = strtotime( 'monday last week' );
					$sales_by_date->end_date = strtotime( 'sunday last week' );
				}
				
				if( $filter == 'chartorder-this-month' ) {
					$sales_by_date->start_date = strtotime("first day of this month");
					$sales_by_date->end_date = strtotime("last day of this month");
				}
				
				if( $filter == 'chartorder-last-month' ) {
					$sales_by_date->start_date = strtotime("first day of last month");
					$sales_by_date->end_date = strtotime("last day of last month");
				}
			}

			$sales_by_date->chart_groupby  = 'day';
			$sales_by_date->group_by_query = 'YEAR(posts.post_date), MONTH(posts.post_date), DAY(posts.post_date)';

			wp_send_json($sales_by_date->get_report_status_data());
		}
		
		public function rest_live_search_callback( WP_REST_Request $request ) {
			global $wpdb;
			$json = array();
			$search = str_replace('#', '', $request->get_param( 'search' ));
			
			
			if($search) {
				$query = array();
				$query['fields'] = "SELECT * FROM {$wpdb->posts} as posts";
				$query['where'] = "WHERE posts.post_type = 'shop_order' AND posts.ID = '". $search ."'";
				//$query['where'] .= " AND posts.post_status = 'wc-completed'";
				

				$orders = $wpdb->get_results(implode(' ', $query));
				$json['complete'] = true;
				
				$html = '';
				if( $orders ) {
					
					foreach( $orders as $k => $order ) {
						$_order = new WC_Order($order->ID);
						$order_data = $_order->get_data();

						$html .= '<li><a href="'. admin_url('post.php?post='.$order->ID.'&action=edit') .'">
								<h4>Order ID #'. $order->ID .'</h4>
								<p>Total: '. wc_price($order_data['total']) .', Status: ' . nbd_get_order_status(str_replace('wc-', '', $order->post_status)) .'</p>
							</a>
						</li>';
					}
				}else {
					$html = '<li><p class="search-empty-rs">' . __('No results.', 'nb-dashboard') .'</p></li>';
				}
				
				$json['html'] = $html;
			}
			
			
			
			wp_send_json($json);
			
		}
		
		public function rest_product_sales_callback( WP_REST_Request $request ) {
			$json = array();
			$filter = $request->get_param( 'filter' );
			$range = $request->get_param( 'range' );
			
			if( $filter == 'this-week' ) {
				$json = json_decode(nbd_show_productsales_thisweek());
			}
			
			if( $filter == 'last-week' ) {
				$json = json_decode(nbd_show_productsales_lastweek());
			}
			
			if( $filter == 'this-month' ) {
				$json = json_decode(nbd_show_productsales_thismonth());
			}
			
			if( $filter == 'last-month' ) {
				$json = json_decode(nbd_show_productsales_lastmonth());
			}
			
			if( $range ) {
				$range_day = explode('-', $filter);
				
				if( count($range_day) == 2) {
					$json = json_decode(nbd_show_productsales_range($range_day));
				}

			}
			
			wp_send_json($json);
		}
		
		public function rest_cf7field_callback( WP_REST_Request $request ) {
			$cf7id = (int) $_REQUEST['id'];
			$contact_form = wpcf7_contact_form( $cf7id );
			
			$tags = $contact_form->scan_form_tags();
			$all_tags_name = '<option value="">'.__('Select a field', 'nb-dashboard').'</option>';
			foreach ($tags as $tag_inner):
				if( $tag_inner['name'] ) {
					$all_tags_name .= '<option value="'. $tag_inner['name'] .'">'. $tag_inner['name'] .'</option>';
				}
			endforeach;
			
			if( $all_tags_name ) {
				$json['complete'] = true;
				$json['fields'] = $all_tags_name;
			}
			
			wp_send_json($json);
			
		}
		
        public function rest_logout_callback( WP_REST_Request $request )
        {
            $json = array();

            wp_logout();
            $url = wp_login_url();

            $json['complete'] = true;
            $json['location'] = $url;

            wp_send_json($json);
        }
		
		
		
        public function rest_read_notification_callback( WP_REST_Request $request )
        {
			$json = array();
			$nb_notification_lists = get_option('nb_notification_lists');
			if( is_array( $nb_notification_lists ) && $nb_notification_lists ) {
				foreach( $nb_notification_lists as $k => $v ) {
					$nb_notification_lists[$k]['read'] = true;
				}

				update_option('nb_notification_lists', $nb_notification_lists);
				update_option('nb_notification_unread', 0);
				
				$json['complete'] = true;
				$json['count'] = 0;
			}
			
			wp_send_json($json);

		}
	}
	
	new NBD_Ajax();

}





